import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register', {
      user: { ...user}
    }).toPromise()
  }

  login(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', {
      user: { ...user}
    }).toPromise()
  }


}



