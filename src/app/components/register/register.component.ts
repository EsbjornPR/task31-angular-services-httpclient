import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    username: '',
    password: ''
  };

  public repeatedPassword: string = '';

  constructor(private AuthService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }


  async onSaveClicked() {
    try {
      if (this.user.username == '' || this.user.password == '' || this.user.password != this.repeatedPassword) {
        alert('Fill in all fields correctly please!')
      } else {
        const result: any = await this.AuthService.register(this.user);
        console.log('Resultat från API');
        console.log(result);
        this.router.navigateByUrl('/login');
      }
    } catch (e) {
      console.error(e);
    }

  }

  onCancelClicked() {
    this.router.navigateByUrl('/login');
  }

}
