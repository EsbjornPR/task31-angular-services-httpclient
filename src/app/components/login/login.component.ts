import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    username: '',
    password: ''
  };

  constructor(private AuthService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  async onLoginClicked() {
    try {
      const result: any = await this.AuthService.login(this.user);
      if ( result.status==200 ) {
        console.log('Successful login')
        this.router.navigateByUrl('/dashboard');        
      } else {
        alert('Login attempt failed');
      }
    } catch (e) {
      console.error(e);
    }
  }

  onRegisterClicked() {
    this.router.navigateByUrl('/register');
  }

}
